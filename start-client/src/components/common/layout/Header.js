import React from 'react'

import HeaderMobile from './HeaderMobile'
import Logo from './Logo'

const Header = () => (
  <header id='header'>
    <div className='not-mobile'>
      <h1 className='logo'>
        <span>
            <p>You can send your feedback to nikolaiko@mail.ru</p>
        </span>
      </h1>
    </div>
    <HeaderMobile />
  </header>
)

export default Header
